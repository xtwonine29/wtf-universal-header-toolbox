var logoType = [
    "logo-center",
    "logo-sidebar"
];

var logoIconSize = [
    "logo-icon-md",
    "logo-icon-lg"
];

var logoTextSize = [
    "fs-1",
    "fs-2",
    "fs-3"
];

var logoBorderType = [
    "logo-side-borders"
];

var colors = [
    "sc-w",
    "sc-b",
    "sc-d",
    "sc-m",
    "sc-ml",
    "lc-w",
    "lc-b",
    "lc-d",
    "lc-m",
    "lc-ml"
];

var navlineType = [
    "wnd-n-standard",
    "wnd-n-subclaim",
    // "nav-standard-sliding",
    // "nav-standard-subclaim-sliding",
    "wnd-n-centered",
    "wnd-n-sidebar"
    // "nav-standard",
    // "nav-standard-subclaim",
    // "nav-standard-sliding",
    // "nav-standard-subclaim-sliding",
    // "nav-centered"
];

var menuTypeClasses = [
    "wnd-mt-classic",
    "wnd-mt-sliding",
    "wnd-mt-boxed",
    "wnd-mt-sidebar"
];

var navlineTransparencyClasses = [
    "wnd-n-bg",
    "wnd-n-transparent"
];

var navlineFixedClasses = [
    "wnd-nav-fixed"
];

var navlineHeightClasses = [
    "wnd-nh-s",
    "wnd-nh-m",
    "wnd-nh-l"
];

var navlineAlignClasses = [
    "wnd-na-t",
    "wnd-na-c",
    "wnd-na-b"
];

var navlineBackgroundClasses = [
    "wnd-n-bg-n",
    "wnd-n-bg-l",
    "wnd-n-bg-d"
];

var widthClasses = [
    "wnd-w-narrow",
    "wnd-w-default",
    "wnd-w-wider",
    "wnd-w-wide",
    "wnd-w-full",
    "wnd-w-max"
];

var contentHeightClasses = [
    "wnd-h-h",
    "wnd-h-a",
    "wnd-h-50",
    "wnd-h-70",
    "wnd-h-f",
    "wnd-h-fa"
];

var contentAlignClasses = [
    "wnd-p-tl",
    "wnd-p-tc",
    "wnd-p-tr",
    "wnd-p-cl",
    "wnd-p-cc",
    "wnd-p-cr",
    "wnd-p-bl",
    "wnd-p-bc",
    "wnd-p-br"
];

var headerContentAlignClasses = [
    "wnd-ca-l",
    "wnd-ca-c",
    "wnd-ca-r"
];

var headerBorder = [
    "wnd-lb-s",
    "wnd-lb-m",
    "wnd-lb-l",
    "wnd-header-border-s",
    "wnd-header-border-m",
    "wnd-header-border-l"
];

var headerContentContainerClasses = [
    "wnd-cw-f",
    "wnd-cw-50",
    "wnd-cw-70"
];

var menuGapClasses = [
    "wnd-mg-s",
    "wnd-mg-m",
    "wnd-mg-l"
];

var menuDividerClasses = [
    "wnd-md-slash",
    "wnd-md-vertical",
    "wnd-md-dpipe"
];

var menuHoverClasses = [
    "wnd-mh-strikethrough",
    "wnd-mh-shadow",
    "wnd-mh-additive",
    "wnd-mh-darken",
    "wnd-mh-lighten",
    "wnd-mh-underline-keepcolor",
    "wnd-mh-underline-additive",
    "wnd-mh-underline-doublepipe"
];

var borderTransitionClasses = [
    "wnd-mht-none",
    "wnd-mht-opacity",
    "wnd-mht-grow"
];

var boxBackgroundClasses = [
    "wnd-cb-bg-n",
    "wnd-cb-bg-l",
    "wnd-cb-bg-d"
];

var boxPaddingClasses = [
    "wnd-cb-p-s",
    "wnd-cb-p-m",
    "wnd-cb-p-l"
];

var boxBorderClasses = [
    "wnd-cb-b-none",
    "wnd-cb-b-solid",
    "wnd-cb-b-dashed",
    "wnd-cb-b-dotted"
];

var headerGradientClasses = [
    "wnd-l-hg-on",
    "wnd-l-hg-off"
]

var navlineTransparencyClasses = [
    "wnd-n-bg",
    "wnd-n-transparent"
]

var addSectionControls = function(){

    $(".s").each(function(){
        $(this).append('<div class="section-transparent" style="width: 30px; height: 30px; background: red; cursor: pointer; position: absolute; top: 0; right: 0;"></div>');

    });

    $(".section-transparent").on("click", function(e){
        if($(this).closest(".s").hasClass("sc-t")){
            $(this).closest(".s").removeClass(colors.join(" ")).addClass("sc-w");
        }else{
            $(this).closest(".s").removeClass(colors.join(" ")).addClass("sc-t").addClass($(".l-bg").attr("class").replace("l-bg ", ""));
        }

    });

}

var applyStyles = function(styleData){

    console.log("APPLY");

    setTimeout(function(){



        // $(".h-h").removeClass(logoType.join(" ")).addClass(styleData.logoType);
        $(".h-h").removeClass(logoIconSize.join(" ")).addClass(styleData.logoIconSize);
        $(".h-h").removeClass(logoTextSize.join(" ")).addClass(styleData.logoTextSize);
        $(".h-h").removeClass(logoBorderType.join(" ")).addClass(styleData.logoBorderType);


        console.log("COLOR: " + styleData.layoutBgColor);
        $(".l-bg").removeClass(colors.join(" ")).addClass(styleData.layoutBgColor);
        // $(".l-bg").addClass(styleData.layoutBgColor);

        // ---- STYLY KTERE MUSI BYT PRITOMNE NA OBOU BLOCICH HLAVICKY -------------------------------------------------

        $(".h-h, .h-m").removeClass(navlineType.join(" ")).addClass(styleData.navType);

        if(styleData.navType === "wnd-n-sidebar"){
            $("body").addClass("wnd-n-sidebar");
            $(".h-h").removeClass(logoType.join(" ")).addClass("logo-sidebar");
            // $(".h-h").removeClass(menuTypeClasses.join(" ")).addClass("wnd-menu-sidebar");
        }else {
            $("body").removeClass("wnd-n-sidebar");
            // $(".h-h").removeClass(menuTypeClasses.join(" ")).addClass("wnd-menu-classic");
            if (styleData.navType === "wnd-n-centered") {
                $(".h-h").removeClass(logoType.join(" ")).addClass("logo-center");
            }
        }

        // --------


        setTimeout(function(){

            if(styleData.navType === "wnd-n-centered"){
                $(".h-m").css("paddingTop", parseInt($(".h-h").outerHeight()) - parseInt($(".h-h").css("borderTopWidth"))  );

                $(".h-m").css("marginTop", parseInt($(".h-h").outerHeight()) * -1);

                if(styleData.navTransparency !== "wnd-n-transparent"){
                    $(".h-m .s-bg").css("top", parseInt($(".h-h").outerHeight()) - parseInt($(".h-h").css("borderTopWidth"))  );
                }

            }else{
                $(".h-m").attr("style", "");
            }

        }, 500);

        $(".h-h, .h-m").removeClass(menuTypeClasses.join(" ")).addClass(styleData.menuType);
        // handle heights
        $(".h-h, .h-m").removeClass(navlineHeightClasses.join(" ")).addClass(styleData.navHeight);

        // navline transparency, hairdresser gradient
        $(".h-h, .h-m").removeClass(headerGradientClasses.join(" ")).addClass(styleData.headerGradient);
        $(".h-h, .h-m").removeClass(navlineTransparencyClasses.join(" ")).addClass(styleData.navTransparency);
        if(styleData.navTransparency === "wnd-n-transparent"){
            $("body").addClass("wnd-n-transparent");

            console.log("wtf");

            $(".h-h").removeClass(colors.join(" "));

            if($(".h-m").hasClass("sc-w")){ $(".h-h").addClass("sc-w"); }
            if($(".h-m").hasClass("sc-b")){ $(".h-h").addClass("sc-b"); }
            if($(".h-m").hasClass("sc-d")){ $(".h-h").addClass("sc-d"); }
            if($(".h-m").hasClass("sc-m")){ $(".h-h").addClass("sc-m"); }
            if($(".h-m").hasClass("sc-ml")){ $(".h-h").addClass("sc-ml"); }


        }else{
            $("body").removeClass("wnd-n-transparent");
        }


        $(".h-h, .h-m").removeClass(navlineFixedClasses.join(" ")).addClass(styleData.navFixed);


        // $(".h-h, .h-m").removeClass(headerBorder.join(" ")).addClass(styleData.headerBorder);
        $("body").removeClass(headerBorder.join(" ")).addClass(styleData.headerBorder);

        // umisteni na spodek
        $(".h-h, .h-m").removeClass("wnd-np-bottom").addClass(styleData.navBottom);
        $(".h-h, .h-m").removeClass("wnd-boxed").addClass(styleData.headerBoxed);
        if(styleData.headerBoxed === "wnd-boxed"){
            $("body").addClass("wt-boxed-bg");
            // $(".l-bg").removeClass(colors.join(" "));
        }else{
            $("body").removeClass("wt-boxed-bg");
        }

        // $(".h-h").prepend("<div class=\"top-bar\"></div>");
        $("body").removeClass("wnd-n-tb-on wnd-n-tb-off").addClass(styleData.navTopbar);


        // ---- STYLY OVLIVNUJICI POUZE NAVLINE ------------------------------------------------------------------------

        // handle widths
        $(".h-h").removeClass(widthClasses.join(" ")).addClass(styleData.navWidth);

        // handle align
        $(".h-h").removeClass(navlineAlignClasses.join(" ")).addClass(styleData.navAlign);

        $(".h-h").removeClass(menuGapClasses.join(" ")).addClass(styleData.menuGaps);
        $(".h-h").removeClass(menuDividerClasses.join(" ")).addClass(styleData.menuDividers);
        $(".h-h").removeClass(menuHoverClasses.join(" ")).addClass(styleData.menuHovers);
        $(".h-h").removeClass(borderTransitionClasses.join(" ")).addClass(styleData.borderTransition);



        $(".h-h").removeClass(navlineBackgroundClasses.join(" ")).addClass(styleData.navBackground);

        if(styleData.navBackground === "wnd-n-bg-l"){
            $(".h-h").removeClass(colors.join(" ")).addClass("sc-ml");
        }else if(styleData.navBackground === "wnd-n-bg-d"){
            $(".h-h").removeClass(colors.join(" ")).addClass("sc-m");
        }else{
            // $(".h-h").removeClass(colors.join(" ")).addClass("sc-w");
        }

        // ---- STYLY OVLIVNUJICI POUZE MAIN HEADER --------------------------------------------------------------------

        // handle #main content part height
        $(".h-m").removeClass(contentHeightClasses.join(" ")).addClass(styleData.contentHeight);

        setTimeout(function(){
            if(styleData.navBottom === "wnd-np-bottom" && styleData.contentHeight !== "wnd-h-h"){

                var cmsBarHeight = $("#wnd_toolbar").length ? parseInt($("#wnd_toolbar").outerHeight()) : 0;
                var topBarHeight = $(".top-bar").css("display") === "block" ? parseInt($(".top-bar").outerHeight()) : 0;
                var borderWidthShift = styleData.navTransparency === "wnd-n-transparent" ? parseInt($(".h-m").css("borderTopWidth")) : 0;
                var mainHeaderHeight = parseInt($(".h-m").outerHeight()) - parseInt($(".h-h").outerHeight()) - borderWidthShift + topBarHeight + cmsBarHeight;

                console.log("cmsBarHeight " + cmsBarHeight);
                console.log("topBarHeight " + topBarHeight);
                console.log("borderWidthShift " + borderWidthShift);
                console.log("h-m outer  " + parseInt($(".h-m").outerHeight()));
                console.log("h-h outer " + parseInt($(".h-h").outerHeight()));
                console.log("mainHeaderHeight " + mainHeaderHeight);

                // $(".h-h").css("position", "absolute").css("bottom", "0");
                $(".h-h").css("position", "absolute").css("top", mainHeaderHeight);
            }else{
                $(".h-h").attr("style", "");
            }
        }, 10);

        // handle #main content part width
        console.log("content width " + styleData.contentWidth);
        $(".s").removeClass(widthClasses.join(" ")).addClass(styleData.contentWidth);

        // handle #main content part align
        $(".h-m").removeClass(contentAlignClasses.join(" ")).addClass(styleData.contentAlign);

        $(".h-m").removeClass(headerContentAlignClasses.join(" ")).addClass(styleData.headerContentAlign);
        $(".h-m").removeClass(headerContentContainerClasses.join(" ")).addClass(styleData.headerContentContainer);

        $(".h-m").removeClass(boxBackgroundClasses.join(" ")).addClass(styleData.boxBackground);

        $(".h-m .h-c-b").removeClass("c-o sc-w sc-d");
        if(styleData.boxBackground === "wnd-cb-l"){
            $(".h-m .h-c-b").addClass("c-o sc-w");
        }else if(styleData.boxBackground === "wnd-cb-d"){
            $(".h-m .h-c-b").addClass("c-o sc-d");
        }

        $(".h-m").removeClass(boxPaddingClasses.join(" ")).addClass(styleData.boxPadding);
        $(".h-m").removeClass(boxBorderClasses.join(" ")).addClass(styleData.boxBorder);

        $("body").addClass("helpers-loaded");


    }, 200);

};

chrome.runtime.onMessage.addListener(
    function(message, sender, sendResponse) {
        switch(message.type) {
            case "applyStyles":
                applyStyles(message.styleData);
                sendResponse("Done applying styles");
                break;
        }
    }
);

$(document).ready(function(){
    chrome.storage.sync.get(['wtf-styles-data', 'wtf-auto-apply'], function (items) {
        addSectionControls();
        if(typeof items['wtf-auto-apply'] !== 'undefined') {
            applyStyles(items['wtf-styles-data']);
        }
    })
});
