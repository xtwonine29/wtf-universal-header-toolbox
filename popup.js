$(document).ready(function(){






    $("#status").html("Hello");

    // setting the saved/default values when popup is opened

    chrome.storage.sync.get(['wtf-styles-data', 'wtf-auto-apply'], function(items){
        // console.log("Saved: " + items['wtf-styles-data']);
        // console.log("Autopaint: " + items['wtf-auto-apply']);

        if(typeof items['wtf-styles-data'] !== 'undefined'){
            $('#layout-bg').val(items['wtf-styles-data'].layoutBgColor);
            $('#navline-type').val(items['wtf-styles-data'].navType);
            $('#menu-type').val(items['wtf-styles-data'].menuType);
            $('#header-boxed').val(items['wtf-styles-data'].headerBoxed);
            $('#navline-topbar').val(items['wtf-styles-data'].navTopbar);
            $('#navline-transparency').val(items['wtf-styles-data'].navTransparency);
            $('#header-gradient').val(items['wtf-styles-data'].headerGradient);
            $('#navline-fixed').val(items['wtf-styles-data'].navFixed);
            $('#navline-height').val(items['wtf-styles-data'].navHeight);
            $('#menu-gaps').val(items['wtf-styles-data'].menuGaps);
            $('#menu-dividers').val(items['wtf-styles-data'].menuDividers);
            $('#menu-hovers').val(items['wtf-styles-data'].menuHovers);
            $('#border-transitions').val(items['wtf-styles-data'].borderTransition);
            $('#header-border').val(items['wtf-styles-data'].headerBorder);
            $('#navline-width').val(items['wtf-styles-data'].navWidth);
            $('#navline-align').val(items['wtf-styles-data'].navAlign);
            $('#navline-bg').val(items['wtf-styles-data'].navBackground);
            $('#header-height').val(items['wtf-styles-data'].contentHeight);
            $('#header-width').val(items['wtf-styles-data'].contentWidth);
            $('#header-align').val(items['wtf-styles-data'].contentAlign);
            $('#header-content-align').val(items['wtf-styles-data'].headerContentAlign);
            $('#header-content-container').val(items['wtf-styles-data'].headerContentContainer);
            $('#header-box-bg').val(items['wtf-styles-data'].boxBackground);
            $('#header-box-padding').val(items['wtf-styles-data'].boxPadding);
            $('#header-box-border').val(items['wtf-styles-data'].boxBorder);
            $('#navline-bottom').val(items['wtf-styles-data'].navBottom);
            $('#logo-type').val(items['wtf-styles-data'].logoType);
            $('#logo-icon-size').val(items['wtf-styles-data'].logoIconSize);
            $('#logo-text-size').val(items['wtf-styles-data'].logoTextSize);
            $('#logo-border-type').val(items['wtf-styles-data'].logoBorderType);
        }

        if(typeof items['wtf-auto-apply'] !== 'undefined'){
            $("#styles-autoapply").attr("checked", true);
        }


        if($("#navline-transparency").val() !== "wnd-nav-transparent"){
            $("#navline-bg").hide();
            $('label[for="navline-bg"]').hide();
        }else{
            $("#navline-bg").show();
            $('label[for="navline-bg"]').show();
        }
    });

    // binding everything

    $("#logo-type, #logo-icon-size, #logo-text-size, #logo-border-type, #layout-bg, #navline-type, #navline-transparency, #navline-fixed, #navline-height, #header-border, #navline-width, #navline-align, #header-height, #header-width, #header-align, #header-content-align, #header-content-container, #menu-gaps, #menu-dividers, #menu-hovers, #border-transitions, #header-box-bg, #header-box-padding, #header-box-border, #navline-bg, #navline-bottom, #navline-topbar, #menu-type, #header-boxed, #header-gradient").on("change", function(e){
        console.log("change");

        if($("#navline-type").val() === "nav-sidebar"){
            $("#menu-type").val("wnd-menu-sidebar");
        }else if($("#navline-type").val() === "nav-centered" && $("#menu-type").val() !== "wnd-menu-classic"){
            $("#menu-type").val("wnd-menu-classic");
        }else if($("#navline-type").val() === "nav-standard" || ($("#navline-type").val() === "nav-standard-classic")){
            $("#menu-type").val("wnd-menu-classic");
        }

        if($("#navline-transparency").val() !== "wnd-nav-transparent"){
            $("#navline-bg").hide();
        }else{
            $("#navline-bg").show();
        }

        chrome.storage.sync.set(
            {'wtf-styles-data': {
                layoutBgColor: $("#layout-bg").val(),
                navType: $("#navline-type").val(),
                menuType: $("#menu-type").val(),
                headerBoxed: $("#header-boxed").val(),
                navTopbar: $("#navline-topbar").val(),
                navTransparency: $("#navline-transparency").val(),
                headerGradient: $("#header-gradient").val(),
                navFixed: $("#navline-fixed").val(),
                navHeight: $("#navline-height").val(),
                menuGaps: $("#menu-gaps").val(),
                menuDividers: $("#menu-dividers").val(),
                menuHovers: $("#menu-hovers").val(),
                borderTransition: $("#border-transitions").val(),
                headerBorder: $("#header-border").val(),
                navWidth: $("#navline-width").val(),
                navAlign: $("#navline-align").val(),
                navBackground: $("#navline-bg").val(),
                contentHeight: $("#header-height").val(),
                contentWidth: $("#header-width").val(),
                contentAlign: $("#header-align").val(),
                headerContentAlign: $("#header-content-align").val(),
                headerContentContainer: $("#header-content-container").val(),
                boxBackground: $("#header-box-bg").val(),
                boxPadding: $("#header-box-padding").val(),
                boxBorder: $("#header-box-border").val(),
                navBottom: $("#navline-bottom").val(),
                    logoType: $("#logo-type").val(),
                    logoIconSize: $("#logo-icon-size").val(),
                    logoTextSize: $("#logo-text-size").val(),
                    logoBorderType: $("#logo-border-type").val(),
            }
        }, function() {
            $("#status").html('Class config saved');
            console.log("Saved: " + {
                layoutBgColor: $("#layout-bg").val(),
                navType: $("#navline-type").val(),
                menuType: $("#menu-type").val(),
                headerBoxed: $("#header-boxed").val(),
                navTopbar: $("#navline-topbar").val(),
                navTransparency: $("#navline-transparency").val(),
                headerGradient: $("#header-gradient").val(),
                navFixed: $("#navline-fixed").val(),
                navHeight: $("#navline-height").val(),
                menuGaps: $("#menu-gaps").val(),
                menuDividers: $("#menu-dividers").val(),
                menuHovers: $("#menu-hovers").val(),
                borderTransition: $("#border-transitions").val(),
                headerBorder: $("#header-border").val(),
                navWidth: $("#navline-width").val(),
                navAlign: $("#navline-align").val(),
                navBackground: $("#navline-bg").val(),
                contentHeight: $("#header-height").val(),
                contentWidth: $("#header-width").val(),
                contentAlign: $("#header-align").val(),
                headerContentAlign: $("#header-content-align").val(),
	            headerContentContainer: $("#header-content-container").val(),
                boxBackground: $("#header-box-bg").val(),
                boxPadding: $("#header-box-padding").val(),
                boxBorder: $("#header-box-border").val(),
                navBottom: $("#navline-bottom").val(),
                logoType: $("#logo-type").val(),
                logoIconSize: $("#logo-icon-size").val(),
                logoTextSize: $("#logo-text-size").val(),
                logoBorderType: $("#ogo-border-type").val(),
            });

        });
    });

    $("#styles-autoapply").on("click", function(e){
                console.log("Autoapply state: " + ($("#styles-autoapply").is(":checked")));
        if($("#styles-autoapply").is(":checked")){
            chrome.storage.sync.set({'wtf-auto-apply': true}, function() {
                $("#status").html('Autoapply engaged');
            });
        }else{
            chrome.storage.sync.remove('wtf-auto-apply', function() {
                $("#status").html('Autoapply disengaged');
            });
        }
    });

    $("#styles-submit").on("click", function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        $("#status").html("Working...");



        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            chrome.tabs.sendMessage(tabs[0].id, {
                type:"applyStyles",
                styleData: {
                    layoutBgColor: $("#layout-bg").val(),
                    navType: $("#navline-type").val(),
                    menuType: $("#menu-type").val(),
                    headerBoxed: $("#header-boxed").val(),
                    navTopbar: $("#navline-topbar").val(),
                    navTransparency: $("#navline-transparency").val(),
                    headerGradient: $("#header-gradient").val(),
                    navFixed: $("#navline-fixed").val(),
                    navHeight: $("#navline-height").val(),
                    menuGaps: $("#menu-gaps").val(),
                    menuDividers: $("#menu-dividers").val(),
                    menuHovers: $("#menu-hovers").val(),
                    borderTransition: $("#border-transitions").val(),
                    headerBorder: $("#header-border").val(),
                    navWidth: $("#navline-width").val(),
                    contentWidth: $("#header-width").val(),
                    navAlign: $("#navline-align").val(),
                    navBackground: $("#navline-bg").val(),
                    contentHeight: $("#header-height").val(),
                    contentAlign: $("#header-align").val(),
                    headerContentAlign: $("#header-content-align").val(),
	                headerContentContainer: $("#header-content-container").val(),
                    boxBackground: $("#header-box-bg").val(),
                    boxPadding: $("#header-box-padding").val(),
                    boxBorder: $("#header-box-border").val(),
                    navBottom: $("#navline-bottom").val(),
                    logoType: $("#logo-type").val(),
                    logoIconSize: $("#logo-icon-size").val(),
                    logoTextSize: $("#logo-text-size").val(),
                    logoBorderType: $("#ogo-border-type").val(),
                }
            }, function(response) {
                $("#status").html(response);
            })
        })
    })
});
